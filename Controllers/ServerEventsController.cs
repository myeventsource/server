using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using server.Models;

[Route("event-source")]
[EnableCors("eventSourceCORS")]
public class ServerEventsController : Controller
{
    private readonly ILogger<ServerEventsController> _logger;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public ServerEventsController(ILogger<ServerEventsController> logger, IHttpContextAccessor httpContextAccessor)
    {
        _logger = logger;
        _httpContextAccessor = httpContextAccessor;
    }

    [HttpGet("test")]
    public string Test()
    {
        var eventResponse = new EventResponseModel();
        return JsonSerializer.Serialize(eventResponse);
    }

    [HttpGet("stream")]
    public async Task Get()
    {
        var response = _httpContextAccessor.HttpContext.Response;
        response.Headers.Add("Content-Type", "text/event-stream");
        response.StatusCode = 200;

        var eventResponse = new EventResponseModel();

        for (var i = 1; i < 1000; i++)
        {
            await response.WriteAsync($"data: {eventResponse.RandomText} - {eventResponse.TimeStamp} \r\r");
            await response.Body.FlushAsync();
            await Task.Delay(1000);
        }
    }
}
