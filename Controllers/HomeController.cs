﻿using Microsoft.AspNetCore.Mvc;

namespace server.Controllers
{
    [Route("/")]
    [Route("home")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Ok("simple streaming API demo");
        }
    }
}
