using System;
using System.Linq;

namespace server.Models
{
    public class EventResponseModel
    {
        private static Random _random;
        private string[] paragraphText =
        {
            "To Be the Light of Someone’s Life: Love is a powerful emotion that can become a person’s reason for living.",
            "The Best Things in Life are Free: This applies to things in life that are exceptionally beautiful or delightful but are also free.",
            "Lead a Double Life: People go to great lengths to hide activities that are taboo, and they work hard to seem normal.",
            "Life is Just a Bowl of Cherries: This used when things are going well. However, it is also used sarcastically when things are not going so well.",
            "Spring to Life: This implies that an object, person or thing abruptly became active",
            "Life in the Fast Lane: Some people prefer a wild, dangerous or carefree life. ",
            "He seems to lead a charmed Life: It means that person always seems to have good luck and good opportunities."
        };

        public EventResponseModel()
        {
            _random = new Random();
        }

        private string GetNextParagraph()
        {
            int index = _random.Next(paragraphText.Length);
            return paragraphText[index];
        }

        public string RandomText { get { return GetNextParagraph(); } }

        public string TimeStamp { get { return DateTime.Now.ToString("hh:mm:ss tt"); } }
    }
}
